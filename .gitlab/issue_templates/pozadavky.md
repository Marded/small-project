* [ ] GIT repozitář na Gitlabu.

* [ ] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)

* [ ] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)

* [ ] Vytvořená CI v repozitáři.

* [ ] CI má minimálně dvě úlohy:

* [ ] Test kvality Markdown stránek.

* [ ] Generování HTML stránek z Markdown zdrojů.

* [ ] CI má automatickou úlohou nasazení web stránek (deploy).

* [ ] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:
