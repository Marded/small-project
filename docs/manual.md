# "Malá" práce

### **Popis**

Tato webová aplikace slouží k evidenci servisních úkonů a prohlídek jednotlivých aut. Aplikace umožnuje vytvořit několik rolí. Pokud se jedná o *automechanika*, tak ten má jiné práva než např. *administrátor* nebo *zákazník*. Aplikace ulehčuje přehled prováděných zásahů a mít možnost zpětně dohledat dřívější opravy.

### **Instalace a použití**

K instalaci aplikace je potřeba:

1. Instalace knihovny jazzmin
  * Použite: `pip install -U django-jazzmin`
1. Otevřete projekt např. PyCharm
1. V terminálu spustit
  * `python ./manage.py runserver` nebo `python manage.py runserver 0.0.0.0:8000`
  * Druhý příkaz slouží pro spuštění např. pomocí veřejné IP adresy *např. 192.168.45.100:8000*
  * Default je localhost
1. Následně bychom měli vidět úvodní stránku aplikace
1. Admin rozhraní zobrazíme
  * `vaše adresa/admin`

### **Prekvizity**

Před spuštěním aplikace je potřebá nainstalovat následující

* Python (verze 3.10) [Download](https://www.python.org/downloads/)
* Django (verze 5.0.0) [Download](https://www.djangoproject.com/download/)
* Jazzmin [Install](https://django-jazzmin.readthedocs.io/installation/)

### **Licence**

Tento projekt jsem vytvořil v rámci projektu v předmětu SKJ. **Autorem je @marded [DED0070]**

### **Požadavky práce**

V rámci práce by mělo být splněno:

* [x] GIT repozitář na Gitlabu.
  * [x] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [x] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)
* [x] Vytvořená CI v repozitáři.
* [x] CI má minimálně dvě úlohy:
  * [x] Test kvality Markdown stránek.
  * [x] Generování HTML stránek z Markdown zdrojů.
* [x] CI má automatickou úlohou nasazení web stránek (deploy).
* [x] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:

### **Ukázka webu**

![Ukázka aplikace!](images/home.png "Ukázka aplikace")
